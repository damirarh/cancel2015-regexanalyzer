﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestHelper;
using RegexAnalyzer;

namespace RegexAnalyzer.Test
{
    [TestClass]
    public class UnitTest : CodeFixVerifier
    {

        //No diagnostics expected to show up
        [TestMethod]
        public void TestMethod1()
        {
            var test = @"using System.Text.RegularExpressions;

namespace RegExSample
{
    public class Class1
    {
        public void Foo()
        {
            Regex.Match("""", """");
        }
    }
}";

            VerifyCSharpDiagnostic(test);
        }

        //Diagnostic and CodeFix both triggered and checked for
        [TestMethod]
        public void TestMethod2()
        {
            var test = @"using System.Text.RegularExpressions;

namespace RegExSample
{
    public class Class1
    {
        public void Foo()
        {
            Regex.Match("""", ""["");
        }
    }
}";
            var expected = new DiagnosticResult
            {
                Id = "Cancel01",
                Message = String.Format("Regular expression is invalid: {0}", @"parsing ""["" - Unterminated [] set."),
                Severity = DiagnosticSeverity.Error,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 9, 29)
                        }
            };

            VerifyCSharpDiagnostic(test, expected);
        }
//        Test Name:	TestMethod2
//        Test FullName:	RegexAnalyzer.Test.UnitTest.TestMethod2
//        Test Source:	d:\Users\Damir\Git\RegexAnalyzer\RegexAnalyzer\RegexAnalyzer.Test\UnitTests.cs : line 38
//Test Outcome:	Failed
//Test Duration:	0:00:00,0649554

//Result StackTrace:	
//at TestHelper.DiagnosticVerifier.VerifyDiagnosticLocation(DiagnosticAnalyzer analyzer, Diagnostic diagnostic, Location actual, DiagnosticResultLocation expected) in d:\Users\Damir\Git\RegexAnalyzer\RegexAnalyzer\RegexAnalyzer.Test\Verifiers\DiagnosticVerifier.cs:line 195
//   at TestHelper.DiagnosticVerifier.VerifyDiagnosticResults(IEnumerable`1 actualResults, DiagnosticAnalyzer analyzer, DiagnosticResult[] expectedResults) in d:\Users\Damir\Git\RegexAnalyzer\RegexAnalyzer\RegexAnalyzer.Test\Verifiers\DiagnosticVerifier.cs:line 133
//   at TestHelper.DiagnosticVerifier.VerifyDiagnostics(String[] sources, String language, DiagnosticAnalyzer analyzer, DiagnosticResult[] expected) in d:\Users\Damir\Git\RegexAnalyzer\RegexAnalyzer\RegexAnalyzer.Test\Verifiers\DiagnosticVerifier.cs:line 91
//   at TestHelper.DiagnosticVerifier.VerifyCSharpDiagnostic(String source, DiagnosticResult[] expected) in d:\Users\Damir\Git\RegexAnalyzer\RegexAnalyzer\RegexAnalyzer.Test\Verifiers\DiagnosticVerifier.cs:line 44
//   at RegexAnalyzer.Test.UnitTest.TestMethod2() in d:\Users\Damir\Git\RegexAnalyzer\RegexAnalyzer\RegexAnalyzer.Test\UnitTests.cs:line 62
//Result Message:	
//Assert.IsTrue failed.Expected diagnostic to be on line "11" was actually on line "9"

//Diagnostic:
//    // Test0.cs(9,29): error Cancel01: Regular expression is invalid: parsing "[" - Unterminated [] set.
//GetCSharpResultAt(9, 29, RegexAnalyzerAnalyzer.Cancel01)


        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new RegexAnalyzerAnalyzer();
        }
    }
}